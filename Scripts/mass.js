﻿

function add_mass(date, title, group_id, from_chapter, from_num, to_chapter, to_num)
{
    $.ajax({
        type: "POST",
        url: "../AddMass/",
        dataType: "json",
        data: "weekDate=" + date + "&title=" + title + "&groupId=" + group_id + "&fromChapter=" + from_chapter + "&fromNum=" + from_num + "&toChapter=" + to_chapter + "&toNum=" + to_num + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_add_mass(); },
        complete: function (XMLHttpRequest) { ajax_complete_add_mass(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_add_mass(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_add_mass(xml, textStatus); }
    });
}

function ajax_before_send_add_mass()
{
}

function ajax_complete_add_mass(XMLHttpRequest)
{
}

function ajax_error_add_mass(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_add_mass(result, textStatus)
{
    window.open("./?datepicker=" + result, "_self");
}


function update_mass(mass_id, title, group_id, from_chapter, from_num, to_chapter, to_num)
{
    $.ajax({
        type: "POST",
        url: "../UpdateMass/",
        dataType: "json",
        data: "MassId=" + mass_id + "&title=" + title + "&groupId=" + group_id + "&fromChapter=" + from_chapter + "&fromNum=" + from_num + "&toChapter=" + to_chapter + "&toNum=" + to_num + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_update_mass(); },
        complete: function (XMLHttpRequest) { ajax_complete_update_mass(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_update_mass(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_update_mass(xml, textStatus); }
    });
}

function ajax_before_send_update_mass()
{
}

function ajax_complete_update_mass(XMLHttpRequest)
{
}

function ajax_error_update_mass(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_update_mass(result, textStatus)
{
    window.open("./?datepicker=" + result, "_self");
}


function delete_mass(mass_id)
{
    $.ajax({
        type: "POST",
        url: "../DeleteMass/",
        dataType: "json",
        data: "massId=" + mass_id + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_delete_mass(); },
        complete: function (XMLHttpRequest) { ajax_complete_delete_mass(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_delete_mass(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_delete_mass(xml, textStatus); }
    });
}

function ajax_before_send_delete_mass()
{
}

function ajax_complete_delete_mass(XMLHttpRequest)
{
}

function ajax_error_delete_mass(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_delete_mass(result, textStatus)
{
    window.open("./?datepicker=" + result, "_self");
}

