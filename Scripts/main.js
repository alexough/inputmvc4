﻿
$(document).ready(function()
{
    var today = new Date();
    $("#today_text_span").text(today.getFullYear() + "." + (today.getMonth() + 1) + "." + today.getDate());

    $("#select_pages").change(function ()
    {
        var date = $("#datepicker").val();
        window.open('/Input/Home/' + $(this).val() + "/?datepicker=" + date, '_self');
    });

    $("#datepicker").change(function ()
    {
        window.open('/Input/Home/' + $("#select_pages").val() + "/?datepicker=" + $(this).val(), '_self');
    });
});

function select_page(id)
{
    $("#select_pages").val(id);
}

function view_add_title()
{
    $("#new_title_span").toggle();
    //$("#new_title_text_span").toggle();
}

function cancel_add_title()
{
    $("#new_title_span").toggle();
    //$("#new_title_text_span").toggle();
}


function find_titles(selected_page)
{
    $.ajax({
        type: "GET",
        url: "../ListTitles/",
        dataType: "html",
        data: "page=" + selected_page + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_find_titles(); },
        complete: function (XMLHttpRequest) { ajax_complete_find_titles(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_find_titles(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_find_titles(xml, textStatus); }
    });
}

function ajax_before_send_find_titles()
{
}

function ajax_complete_find_titles(XMLHttpRequest)
{
}

function ajax_error_find_titles(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_find_titles(result, textStatus)
{
    $("#select_titles").html(result);
    $("#select_titles").change(function ()
    {
        //$("#selected_title_id").attr("value", $("#select_titles option:selected").attr("value"));
        var board_title_id = $(this).val();
        if (board_title_id == "")
        {
            $("#div_board").html("");
            return;
        }
        var date = $("#datepicker").val();
        if (date == "")
        {
            alert("편집할 날짜를 먼저 선택해 주십시오");
            $("#select_titles").val('');
            return;
        }
        find_boards(board_title_id, date);
    });
}


function add_title()
{
    var selected_page = $("#select_pages option:selected").attr("value");
    if (selected_page == "")
    {
        alert("Select a page first.");
        return;
    }

    var new_title = $("#text_title").attr("value");
    if (new_title == "")
    {
        alert("Title is not entered.");
        return;
    }

    $.ajax({
        type: "POST",
        url: "../AddTitle/",
        dataType: "html",
        data: "title=" + new_title + "&page=" + selected_page + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_add_title(); },
        complete: function (XMLHttpRequest) { ajax_complete_add_title(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_add_title(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_add_title(xml, textStatus); }
    });
}

function ajax_before_send_add_title()
{
}

function ajax_complete_add_title(XMLHttpRequest)
{
}

function ajax_error_add_title(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_add_title(result, textStatus)
{
    //$("#new_title_text_span").toggle();
    $("#new_title_span").toggle();
    $("#text_title").attr("value", "");

    ajax_success_find_titles(result, textStatus);
}



/************** START OF BOARD OPERAIONS  ************************/

function find_boards(board_title_id, date)
{
    $.ajax({
        type: "GET",
        url: "../ListBoards/",
        dataType: "html",
        data: "boardTitleId=" + board_title_id + "&date=" + date + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_find_boards(); },
        complete: function (XMLHttpRequest) { ajax_complete_find_boards(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_find_boards(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_find_boards(xml, textStatus); }
    });
}

function ajax_before_send_find_boards()
{
}

function ajax_complete_find_boards(XMLHttpRequest)
{
}

function ajax_error_find_boards(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_find_boards(result, textStatus)
{
    $("#div_board").html(result);

    $("img[id^='img_sub_item_view_']").click(function ()
    {
        //alert("sub_" + $(this).attr('item_id'));
        find_sub_boards($(this).attr('item_id'));
    });

    $("img[id^='img_item_edit_']").click(function ()
    {
        //alert("edit_" + $(this).attr('item_id'));
        edit_board($(this).attr('item_id'));
    });

    $("img[id^='img_item_delete_']").click(function ()
    {
        //alert("delete_" + $(this).attr('item_id'));
        var board_id = $(this).attr('item_id');
        if (!confirm("Do you really want to delete this item\n[" + $("#text_item_" + board_id).text().trim() + "]?"))
        {
            return;
        }
        delete_board(board_id);
    });

    $("#img_item_update_0").click(function ()
    {
        //alert("add_" + $(this).attr('item_id'));
        var board_title_id = $("#select_titles").val();
        if (board_title_id == "")
        {
            alert("편집할 제목을 먼저 선택해 주십시오");
            return;
        }

        var date = $("#datepicker").val();
        if (date == "")
        {
            alert("편집할 날짜를 먼저 선택해 주십시오");
            return;
        }

        var item = $("#textarea_0").val();
        if (item == "")
        {
            alert("입력할 내용을 먼저 기입해 주십시오");
            return;
        }

        add_board(board_title_id, date, item);
    });
}


function add_board(board_title_id, date, item)
{
    $.ajax({
        type: "POST",
        url: "../AddBoard/",
        dataType: "html",
        data: "boardTitleId=" + board_title_id + "&date=" + date + "&item=" + item + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_add_board(); },
        complete: function (XMLHttpRequest) { ajax_complete_add_board(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_add_board(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_add_board(xml, textStatus); }
    });
}

function ajax_before_send_add_board()
{
}

function ajax_complete_add_board(XMLHttpRequest)
{
}

function ajax_error_add_board(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_add_board(result, textStatus)
{
    ajax_success_find_boards(result, textStatus);
}


function edit_board(board_id)
{
    $("#text_item_" + board_id).hide();
    $("#edit_item_" + board_id).show();
    $("#to_item_edit_button_div_" + board_id).hide();
    $("#to_item_text_button_div_" + board_id).show();
    $("#img_item_update_" + board_id).click(function ()
    {
        update_board(board_id, $("#textarea_" + board_id).val());
    });
    $("#img_item_cancel_" + board_id).click(function ()
    {
        $("#text_item_" + board_id).show();
        $("#edit_item_" + board_id).hide();
        $("#to_item_edit_button_div_" + board_id).show();
        $("#to_item_text_button_div_" + board_id).hide();
    });
}

function update_board(board_id, item)
{
    $.ajax({
        type: "POST",
        url: "../UpdateBoard/",
        dataType: "html",
        data: "boardId=" + board_id + "&item=" + item + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_update_board(); },
        complete: function (XMLHttpRequest) { ajax_complete_update_board(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_update_board(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_update_board(xml, textStatus); }
    });
}

function ajax_before_send_update_board()
{
}

function ajax_complete_update_board(XMLHttpRequest)
{
}

function ajax_error_update_board(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_update_board(result, textStatus)
{
    ajax_success_find_boards(result, textStatus);
}


function delete_board(board_id)
{
    $.ajax({
        type: "POST",
        url: "../DeleteBoard/",
        dataType: "html",
        data: "boardId=" + board_id + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_delete_board(); },
        complete: function (XMLHttpRequest) { ajax_complete_delete_board(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_delete_board(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_delete_board(xml, textStatus); }
    });
}

function ajax_before_send_delete_board()
{
}

function ajax_complete_delete_board(XMLHttpRequest)
{
}

function ajax_error_delete_board(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_delete_board(result, textStatus)
{
    ajax_success_find_boards(result, textStatus);
}

/************** END OF BOARD OPERAIONS  ************************/



/************** START OF SUB BOARD OPERAIONS  ************************/

function find_sub_boards(board_id)
{
    $.ajax({
        type: "GET",
        url: "../ListSubBoards/",
        dataType: "html",
        data: "boardId=" + board_id + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_find_sub_boards(); },
        complete: function (XMLHttpRequest) { ajax_complete_find_sub_boards(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_find_sub_boards(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_find_sub_boards(xml, textStatus, board_id); }
    });
}

function ajax_before_send_find_sub_boards()
{
}

function ajax_complete_find_sub_boards(XMLHttpRequest)
{
}

function ajax_error_find_sub_boards(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_find_sub_boards(result, textStatus, board_id)
{
    $("#div_sub_board_" + board_id).html(result);
    $("#div_sub_board_" + board_id).show();

    $("img[id^='img_sub_item_edit_']").click(function ()
    {
        //alert("add_" + $(this).attr('sub_item_id'));

        var board_id = $(this).attr('item_id');
        var sub_board_id = $(this).attr('sub_item_id');

        edit_sub_board(board_id, sub_board_id);
    });

    $("img[id^='img_sub_item_delete_']").click(function ()
    {
        //alert("delete_" + $(this).attr('sub_item_id'));
        var board_id = $(this).attr('item_id');
        var sub_board_id = $(this).attr('sub_item_id');
        if (!confirm("Do you really want to delete this sub item\n[" + $("#text_sub_item_" + sub_board_id).text().trim() + "]?"))
        {
            return;
        }
        delete_sub_board(board_id, sub_board_id);
    });

    $("img[id^='img_sub_item_update_" + board_id + "_']").click(function ()
    {
        //alert("edit_" + $(this).attr('sub_item_id'));
        var board_id = $(this).attr('item_id');

        var sub_item = $("#textarea_sub_" + board_id + "_0").val();
        if (sub_item == "")
        {
            alert("입력할 내용을 먼저 기입해 주십시오");
            return;
        }

        add_sub_board(board_id, sub_item);
    });
}


function add_sub_board(board_id, sub_item)
{
    $.ajax({
        type: "POST",
        url: "../AddSubBoard/",
        dataType: "html",
        data: "boardId=" + board_id + "&subItem=" + sub_item + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_add_sub_board(); },
        complete: function (XMLHttpRequest) { ajax_complete_add_sub_board(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_add_sub_board(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_add_sub_board(xml, textStatus, board_id); }
    });
}

function ajax_before_send_add_sub_board()
{
}

function ajax_complete_add_sub_board(XMLHttpRequest)
{
}

function ajax_error_add_sub_board(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_add_sub_board(result, textStatus, board_id)
{
    ajax_success_find_sub_boards(result, textStatus, board_id);
}


function edit_sub_board(board_id, sub_board_id)
{
    $("#text_sub_item_" + sub_board_id).hide();
    $("#edit_sub_item_" + sub_board_id).show();
    $("#to_sub_item_edit_button_div_" + sub_board_id).hide();
    $("#to_sub_item_text_button_div_" + sub_board_id).show();
    $("#img_sub_item_update_" + sub_board_id).click(function ()
    {
        update_sub_board(board_id, sub_board_id, $("#textarea_sub_" + sub_board_id).val());
    });
    $("#img_sub_item_cancel_" + sub_board_id).click(function ()
    {
        $("#text_sub_item_" + sub_board_id).show();
        $("#edit_sub_item_" + sub_board_id).hide();
        $("#to_sub_item_edit_button_div_" + sub_board_id).show();
        $("#to_sub_item_text_button_div_" + sub_board_id).hide();
    });
}


function update_sub_board(board_id, sub_board_id, sub_item)
{
    $.ajax({
        type: "POST",
        url: "../UpdateSubBoard/",
        dataType: "html",
        data: "boardId=" + board_id + "&subBoardId=" + sub_board_id + "&subItem=" + sub_item + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_update_sub_board(); },
        complete: function (XMLHttpRequest) { ajax_complete_update_sub_board(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_update_sub_board(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_update_sub_board(xml, textStatus, board_id); }
    });
}

function ajax_before_send_update_sub_board()
{
}

function ajax_complete_update_sub_board(XMLHttpRequest)
{
}

function ajax_error_update_sub_board(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_update_sub_board(result, textStatus, board_id)
{
    ajax_success_find_sub_boards(result, textStatus, board_id);
}


function delete_sub_board(board_id, sub_board_id)
{
    $.ajax({
        type: "POST",
        url: "../DeleteSubBoard/",
        dataType: "html",
        data: "boardId=" + board_id + "&subBoardId=" + sub_board_id + "&i=" + (new Date()).getTime(),
        processData: false,
        beforeSend: function () { ajax_before_send_delete_sub_board(); },
        complete: function (XMLHttpRequest) { ajax_complete_delete_sub_board(XMLHttpRequest); },
        error: function (XMLHttpRequest, textStatus, errorThrown) { ajax_error_delete_sub_board(XMLHttpRequest, textStatus, errorThrown); },
        success: function (xml, textStatus) { ajax_success_delete_sub_board(xml, textStatus, board_id); }
    });
}

function ajax_before_send_delete_sub_board()
{
}

function ajax_complete_delete_sub_board(XMLHttpRequest)
{
}

function ajax_error_delete_sub_board(XMLHttpRequest, textStatus, errorThrown)
{
    alert("(Ajax error: " + XMLHttpRequest.responseText + ")");
}

function ajax_success_delete_sub_board(result, textStatus, board_id)
{
    ajax_success_find_sub_boards(result, textStatus, board_id);
}

/************** END OF SUB BOARD OPERAIONS  ************************/
