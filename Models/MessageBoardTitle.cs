﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InputMVC4.Models
{
    public class MessageBoardTitle
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}