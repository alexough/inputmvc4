﻿using System.Web.Mvc;

namespace InputMVC4.Helpers
{
    public static class HtmlHelpers
    {
        public static string Truncate(this HtmlHelper helper, string input, int length)
        {
            if (input.Length <= length)
            {
                return input;
            }
            else
            {
                return input.Substring(0, length) + "...";
            }
        }

        public static string DisplayGender(this HtmlHelper helper, char? input)
        {
            if (input == 'M')
            {
                return "남자";
            }
            else if (input == 'F')
            {
                return "여자";
            }
            else
            {
                return "";
            }
        }

        public static string DisplayDate(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("yyyy-MM-dd");
            }

            return "";
        }

        public static string DisplayDateInThreeLetterMonth(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("MMM dd, yyyy").ToUpper();
            }

            return "";
        }

        public static string DisplayDateTime(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("yyyy-MM-dd");
            }

            return "";
        }

        public static string DisplayYear(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("yyyy");
            }

            return "";
        }

        public static string DisplayMonthInThreeLeters(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("MMM").ToUpper();
            }

            return "";
        }

        public static string DisplayDayInTwoDigits(this HtmlHelper helper, System.DateTime? datetime)
        {
            if (datetime.HasValue)
            {
                return System.DateTime.Parse(datetime.ToString()).ToString("dd");
            }

            return "";
        }

        public static string DisplayId(this HtmlHelper helper, int? id)
        {
            if (!id.HasValue || id.Value == -1)
            {
                return "";
            }

            return id.ToString();
        }
    }
}