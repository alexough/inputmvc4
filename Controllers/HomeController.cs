﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InputMVC4.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private StakimDataContext context = null;

        public HomeController()
        {
            this.context = new StakimDataContext(System.Configuration.ConfigurationManager.ConnectionStrings["SQL2005_517772_stakimConnectionString"].ConnectionString);
        }

        public ActionResult Index(string datepicker = "")
        {
            ViewBag.week_date = datepicker;
            return View();
        }

        public ActionResult Main(string datepicker = "")
        {
            ViewBag.week_date = datepicker;
            ViewBag.page_id = "main";

            return View();
        }

        public ActionResult Board(string datepicker = "")
        {
            ViewBag.week_date = datepicker;
            ViewBag.page_id = "board";

            return View();
        }

        public ActionResult School(string datepicker = "")
        {
            ViewBag.week_date = datepicker;
            ViewBag.page_id = "school";

            return View();
        }

        public ActionResult Mass(string datepicker)
        {
            ViewBag.week_date = datepicker;
            ViewBag.page_id = "mass";

            if (datepicker == "") return View();

            List<SelectListItem> group_list = context.BibleGroups
                .Where(g => g.id >= 47 && g.id <= 50)
                .Select(g => new SelectListItem
                {
                    Text = g.name.ToString(),
                    Value = g.id.ToString()
                }).ToList();
            ViewBag.group_list = group_list;

            List<BibleGospel> list = context.BibleGospels
                .Where(b => b.week_date == DateTime.Parse(datepicker))
                .ToList();

            return View(list);
        }

        public ActionResult Upload(string datepicker = "")
        {
            ViewBag.week_date = datepicker;
            ViewBag.page_id = "upload";

            return View();
        }

        //[AllowAnonymous]
        [HttpPost]
        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Upload(DateTime week_date, IEnumerable<HttpPostedFileBase> file)
        {
            ViewBag.week_date = week_date.ToString("MM/dd/yyyy");
            ViewBag.fileName = Request.Files[0].FileName;
            ViewBag.message = "";

            string extName = ".wma";
            int index = Request.Files[0].FileName.LastIndexOf(".");
            if (index > 0)
            {
                extName = Request.Files[0].FileName.Substring(index);
            }

            string fileName = week_date.ToString("MM-dd-yy") + "-sermon" + extName;
            string folderName = "sermons";

            string dir = Request.PhysicalApplicationPath + "..\\resources\\" + folderName;
            //fileUploader.SaveAs(Server.MapPath(folderName) + "\\" + fileName);
            Request.Files[0].SaveAs(dir + "\\" + fileName);

            try
            {
                // set the flag in resource table
                Resource resource = FindResource(week_date);
                if (resource == null)
                {
                    InsertResource(week_date);
                }
                else
                {
                    UpdateResource(resource);
                }
            }
            catch (Exception ex)
            {
                ViewBag.message = ex.Message;
                return View();
            }

            ViewBag.page_id = "upload";
            ViewBag.message = "강론 파일 [" + fileName + "] 이 저장되었습니다.";

            return View();
        }

        public ActionResult ListTitles(string page)
        {
            return View(FindBoardTitles(page));
        }

        public ActionResult AddTitle(string title, string page)
        {
            return View(AddBoardTitle(title, page));
        }

        public ActionResult ListBoards(int boardTitleId, DateTime date)
        {
            return View(FindBoardItems(boardTitleId, date));
        }

        public ActionResult AddBoard(int boardTitleId, DateTime date, string item)
        {
            return View(UpdateBoardItem(boardTitleId, date, -1, item));
        }

        public ActionResult UpdateBoard(int boardId, string item)
        {
            return View(UpdateBoardItem(-1, new DateTime(), boardId, item));
        }

        public ActionResult DeleteBoard(int boardId)
        {
            return View(DeleteBoardItem(boardId));
        }

        public ActionResult ListSubBoards(int boardId)
        {
            ViewBag.board_id = boardId;

            return View(FindSubBoardItems(boardId));
        }

        public ActionResult AddSubBoard(int boardId, string subItem)
        {
            ViewBag.board_id = boardId;

            return View(UpdateSubBoardItem(boardId, -1, subItem));
        }

        public ActionResult UpdateSubBoard(int boardId, int subBoardId, string subItem)
        {
            ViewBag.board_id = boardId;

            return View(UpdateSubBoardItem(boardId, subBoardId, subItem));
        }

        public ActionResult DeleteSubBoard(int boardId, int subBoardId)
        {
            ViewBag.board_id = boardId;

            return View(DeleteSubBoardItem(boardId, subBoardId));
        }

        public JsonResult AddMass(DateTime weekDate, string title, int groupId, int fromChapter, int fromNum, int toChapter, int toNum)
        {
            BibleGospel gospel = new BibleGospel();
            gospel.week_date = weekDate;
            gospel.week_title = title;
            gospel.group_id = groupId;
            gospel.from_chapter = fromChapter;
            gospel.from_num = fromNum;
            gospel.to_chapter = toChapter;
            gospel.to_num = toNum;
            context.BibleGospels.InsertOnSubmit(gospel);
            context.SubmitChanges();

            return Json(gospel.week_date.ToString("MM/dd/yyyy"));
        }

        public JsonResult UpdateMass(int massId, string title, int groupId, int fromChapter, int fromNum, int toChapter, int toNum)
        {
            var query = context.BibleGospels.Where(g => g.id == massId);
            if (query.Count() == 0)
            {
                throw new Exception("No mass with the id [" + massId + "]");
            }

            BibleGospel gospel = query.First();
            gospel.week_title = title;
            gospel.group_id = groupId;
            gospel.from_chapter = fromChapter;
            gospel.from_num = fromNum;
            gospel.to_chapter = toChapter;
            gospel.to_num = toNum;
            context.SubmitChanges();

            return Json(gospel.week_date.ToString("MM/dd/yyyy"));
        }

        public JsonResult DeleteMass(int massId)
        {
            var query = context.BibleGospels.Where(g => g.id == massId);
            if (query.Count() == 0)
            {
                throw new Exception("No mass with the id [" + massId + "]");
            }

            BibleGospel gospel = query.First();
            context.BibleGospels.DeleteOnSubmit(gospel);

            context.SubmitChanges();

            return Json(gospel.week_date.ToString("MM/dd/yyyy"));
        }


        private List<BoardTitle> FindBoardTitles(string page)
        {
            List<BoardTitle> list = context.BoardTitles
                .Where(b => b.page == page)
                .OrderBy(b => b.id)
                .ToList();

            return list;
        }

        private List<BoardTitle> AddBoardTitle(string title, string page)
        {
            BoardTitle boardTitle = new BoardTitle();
            boardTitle.title = title;
            boardTitle.page = page;
            context.BoardTitles.InsertOnSubmit(boardTitle);
            context.SubmitChanges();

            return FindBoardTitles(page);
        }

        /*public JsonResult Titles(string page)
        {
            List<Models.MessageBoardTitle> list = FindBoardTitles(page);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        private List<Models.MessageBoardTitle> FindBoardTitles(string page)
        {
            List<Models.MessageBoardTitle> list = context.BoardTitles
                .Where(b => b.page == page)
                .OrderBy(b => b.id)
                .Select(b => new Models.MessageBoardTitle
                {
                    Id = b.id,
                    Title = b.title
                })
                .ToList();

            return list;
        }

        private List<Models.MessageBoardTitle> AddBoardTitle(string title, string page)
        {
            BoardTitle boardTitle = new BoardTitle();
            boardTitle.title = title;
            boardTitle.page = page;
            context.BoardTitles.InsertOnSubmit(boardTitle);
            context.SubmitChanges();

            return FindBoardTitles(page);
        }*/

        //private List<Board> FindBoardItems(int boardTitleId, DateTime date, string parentId)
        private List<Board> FindBoardItems(int boardTitleId, DateTime date)
        {
            /*if (parentId != null && parentId != "")
            {
                return FindSubBoardItems(int.Parse(parentId));
            }*/

            List<Board> list = context.Boards
                .Where(b => b.board_title_id == boardTitleId && b.week_date == date)
                .OrderBy(b => b.id)
                .ToList();

            return list;
        }

        //private List<Board> UpdateBoardItem(int boardTitleId, DateTime date, string parentId, int id, string newValue)
        private List<Board> UpdateBoardItem(int boardTitleId, DateTime date, int id, string newValue)
        {
            /*if (parentId != null && parentId != "")
            {
                return UpdateSubBoardItem(int.Parse(parentId), id, newValue);
            }*/

            var query = context.Boards.Where(b => b.id == id);

            Board board = null;
            if (query.Count() == 0)
            {
                board = new Board();
                board.board_title_id = boardTitleId;
                board.week_date = date;
                context.Boards.InsertOnSubmit(board);
            }
            else
            {
                board = query.First();
            }

            board.item = newValue;

            context.SubmitChanges();

            //return FindBoardItems(board.board_title_id, board.week_date, parentId);
            return FindBoardItems(board.board_title_id, board.week_date);
        }

        //private List<Board> DeleteBoardItem(int boardTitleId, DateTime date, string parentId, int id)
        private List<Board> DeleteBoardItem(int id)
        {
            /*if (parentId != null && parentId != "")
            {
                return DeleteSubBoardItem(int.Parse(parentId), id);
            }*/

            var query = context.Boards.Where(b => b.id == id);

            if (query.Count() == 0)
            {
                throw new Exception("No item with the id [" + id + "]");
            }

            Board board = query.First();
            context.Boards.DeleteOnSubmit(board);

            context.SubmitChanges();

            //return FindBoardItems(board.board_title_id, board.week_date, parentId);
            return FindBoardItems(board.board_title_id, board.week_date);
        }

        private List<SubBoard> FindSubBoardItems(int boardId)
        {
            List<SubBoard> list = context.SubBoards
                .Where(s => s.board_id == boardId)
                .OrderBy(s => s.id)
                .ToList();

            return list;
        }

        private List<SubBoard> UpdateSubBoardItem(int parentId, int id, string newValue)
        {
            var query = context.SubBoards.Where(s => s.id == id);

            SubBoard subBoard = null;
            if (query.Count() == 0)
            {
                subBoard = new SubBoard();
                subBoard.board_id = parentId;
                subBoard.sub_item = newValue;
                context.SubBoards.InsertOnSubmit(subBoard);
            }
            else
            {
                subBoard = query.First();
            }

            subBoard.sub_item = newValue;

            context.SubmitChanges();

            return FindSubBoardItems(parentId);
        }

        private List<SubBoard> DeleteSubBoardItem(int parentId, int id)
        {
            var query = context.SubBoards.Where(s => s.id == id);

            if (query.Count() == 0)
            {
                throw new Exception("No sub item with the id [" + id + "]");
            }

            SubBoard subBoard = query.First();
            context.SubBoards.DeleteOnSubmit(subBoard);

            context.SubmitChanges();

            return FindSubBoardItems(parentId);
        }

        private Resource FindResource(DateTime dateTime)
        {
            var resourceQuery = from resource in context.Resources
                                where resource.week_date == dateTime
                                select resource;

            if (resourceQuery.Count() == 0) return null;

            return resourceQuery.First();
        }

        private void InsertResource(DateTime dateTime)
        {
            Resource resource = new Resource();
            resource.week_date = dateTime;
            resource.newsletter = 0;
            resource.sermon = 1;
            resource.week_title = FindTitle(dateTime);

            context.Resources.InsertOnSubmit(resource);
            context.SubmitChanges();
        }

        private void UpdateResource(Resource resource)
        {
            resource.sermon = 1;

            context.SubmitChanges();
        }

        private string FindTitle(DateTime dateTime)
        {
            var query = from m in context.Masses
                        where m.week_date == dateTime
                        select m;

            if (query.Count() == 0) return null;

            return query.First().week_name;
        }
    }
}
